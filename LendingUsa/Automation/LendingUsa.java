package Lendingusa;

import static org.testng.Assert.assertNotEqualsDeep;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class LendingUsa {
	
	public static WebDriver driver;
	public static WebDriverWait wait;
	
	@BeforeSuite
	public void Setup(){
		System.setProperty("webdriver.chrome.driver", "C:/automacao/chromedriver.exe");
		driver =  new ChromeDriver();
		wait = new WebDriverWait(driver, 60);
		driver.get("http://lendingusa.dev.lendfoundry.com:9002/");
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);  
		driver.manage().window().maximize();
		
	}
	
	@Test(priority = 0)
	 public void Login() {
		driver.findElement(By.id("username")).sendKeys("ebueno");
		driver.findElement(By.id("password")).sendKeys("ebueno");
		driver.findElement(By.id("log-in")).sendKeys(Keys.ENTER);
		
	}/*
	@Test(priority = 1)
	public void Create() throws InterruptedException {
		//create new application Happy Patch
		driver.findElement(By.cssSelector("[href='#/dtm-application']")).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//button[contains(text(), 'Fill')]")).sendKeys(Keys.ENTER);
		driver.findElement(By.cssSelector("#submit button")).sendKeys(Keys.ENTER);	
		Thread.sleep(22000);
		driver.findElement(By.cssSelector("#host button")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#offerPanel button")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#dropdownBtn")).sendKeys(Keys.SPACE);
		driver.findElement(By.xpath("//a[contains(text(), 'Checking')]")).sendKeys(Keys.ENTER);
		driver.findElement(By.cssSelector("[value='0']")).sendKeys(Keys.SPACE);;
		driver.findElement(By.cssSelector("[class='btn btn-primary style-scope button-primary']")).sendKeys(Keys.ENTER);
		Thread.sleep(17000);
		driver.findElement(By.cssSelector("[name='agreeToTerms'] input")).sendKeys(Keys.SPACE);
		driver.findElement(By.cssSelector("button")).sendKeys(Keys.ENTER);
		Thread.sleep(9000);
		driver.findElement(By.cssSelector("button")).sendKeys(Keys.ENTER);
	}*/
	@Test(priority = 2)
	public void Filter() throws InterruptedException{
		//Filter application.
		driver.findElement(By.cssSelector("[href='#/applications']")).sendKeys(Keys.ENTER);	
		Thread.sleep(7000);
		driver.findElement(By.cssSelector("#table-search")).sendKeys("9,");
		driver.findElement(By.cssSelector("[class='btn btn-primary btn-xs detail-btn']")).click();
		//driver.findElement(By.cssSelector("#alloy-table tbody tr:nth-child(3) td:nth-child(11) span")).click();
		//driver.findElement(By.xpath("//*[@id='alloy-table']/tbody/tr/td[11]/span")).click();
		//driver.findElement(By.cssSelector("[id='alloy-table'] tbody td span")).click();
	}
}
